import { AddAddressComponent } from './add-address/add-address.component';
import { AddressListComponent } from './address-list/address-list.component';
import { RouterModule } from '@angular/router';
export var AppRoutes = [
    { path: 'addresses', component: AddressListComponent },
    { path: 'addAddress', component: AddAddressComponent }
];
export var ROUTING = RouterModule.forRoot(AppRoutes);
//# sourceMappingURL=app.routing.js.map