var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DataService } from './../data.service';
import { ApiService } from './../../swagger/api/api.service';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
var AddAddressComponent = /** @class */ (function () {
    function AddAddressComponent(api, route, router, dataser) {
        this.api = api;
        this.route = route;
        this.router = router;
        this.dataser = dataser;
    }
    AddAddressComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.dataser.currentMessage.subscribe(function (message) { return _this.message = message; });
        console.log(this.message);
        this.api.getDataById(this.message).subscribe(function (success) {
            _this.demo = success;
            _this.countries = _this.demo.countries;
            _this.states = _this.demo.states;
            _this.cities = _this.demo.cities;
            _this.dataser.currantAction.subscribe(function (action) { return _this.action = action; });
        });
    };
    AddAddressComponent.prototype.edit = function () {
        var _this = this;
        this.api.editData(this.message, this.countries, this.states, this.cities).subscribe(function (success) {
            _this.router.navigate(['addresses']);
        }, function (error) {
            console.log(error);
        });
    };
    AddAddressComponent.prototype.addAddress = function () {
        var _this = this;
        this.countries = undefined;
        this.states = undefined;
        this.cities = undefined;
        this.api.createData(this.countries, this.states, this.cities).subscribe(function (success) {
            _this.router.navigate(['addresses']);
        });
    };
    AddAddressComponent = __decorate([
        Component({
            selector: 'app-add-address',
            templateUrl: './add-address.component.html',
            styleUrls: ['./add-address.component.css']
        }),
        __metadata("design:paramtypes", [ApiService, ActivatedRoute, Router, DataService])
    ], AddAddressComponent);
    return AddAddressComponent;
}());
export { AddAddressComponent };
//# sourceMappingURL=add-address.component.js.map