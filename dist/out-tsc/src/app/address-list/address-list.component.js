var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { DataService } from './../data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from './../../swagger/api/api.service';
import { Component } from '@angular/core';
import swal from 'sweetalert2';
var AddressListComponent = /** @class */ (function () {
    function AddressListComponent(api, route, router, dataser) {
        this.api = api;
        this.route = route;
        this.router = router;
        this.dataser = dataser;
    }
    AddressListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.api.getAllData().subscribe(function (success) {
            _this.data = success;
        });
    };
    AddressListComponent.prototype.add = function () {
        this.router.navigate(['addAddress']);
    };
    AddressListComponent.prototype.edit = function (_id) {
        this.dataser.changeMessage(_id);
        console.log(_id);
        this.router.navigate(['addAddress']);
        this.dataser.changeAction('EDIT');
    };
    AddressListComponent.prototype.delete = function (_id) {
        var _this = this;
        swal({
            title: 'Are you sure?',
            text: 'Are you sure that you want to Delete this?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then(function (result) {
            if (result.value) {
                _this.api.deleteData(_id).subscribe(function (sucess) {
                    if (sucess) {
                        window.location.reload();
                    }
                });
            }
        });
    };
    AddressListComponent = __decorate([
        Component({
            selector: 'app-address-list',
            templateUrl: './address-list.component.html',
            styleUrls: ['./address-list.component.css']
        }),
        __metadata("design:paramtypes", [ApiService, ActivatedRoute, Router, DataService])
    ], AddressListComponent);
    return AddressListComponent;
}());
export { AddressListComponent };
//# sourceMappingURL=address-list.component.js.map