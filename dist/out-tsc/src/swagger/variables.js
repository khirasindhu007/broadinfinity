import { InjectionToken } from '@angular/core';
export var BASE_PATH = new InjectionToken('basePath');
export var COLLECTION_FORMATS = {
    'csv': ',',
    'tsv': '   ',
    'ssv': ' ',
    'pipes': '|'
};
//# sourceMappingURL=variables.js.map