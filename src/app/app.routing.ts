import { AddAddressComponent } from './add-address/add-address.component';
import { AddressListComponent } from './address-list/address-list.component';
import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { EditComponent } from './edit/edit.component';

export const AppRoutes: Routes = [
    { path: 'addresses', component: AddressListComponent },
    { path: 'addAddress', component: AddAddressComponent },
    { path: 'edit', component: EditComponent }

  ];

export const ROUTING: ModuleWithProviders = RouterModule.forRoot(AppRoutes);
