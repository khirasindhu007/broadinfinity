import { DataService } from './../data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from './../../swagger/api/api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public countries;
  public states;
  public cities;
  public message;
  public action;
  public demo;
  constructor(public api: ApiService, private route: ActivatedRoute, private router: Router, public dataser: DataService) {}

  ngOnInit() {

    this.dataser.currentMessage.subscribe(message => this.message = message);
    console.log(this.message);
    this.api.getDataById(this.message).subscribe(success => {
      this.demo = success;
      this.countries = this.demo.countries;
      this.states = this.demo.states;
      this.cities = this.demo.cities;

      this.dataser.currantAction.subscribe(action => this.action = action);

    });
  }
  edit() {
    this.api.editData(this.message, this.countries, this.states, this.cities).subscribe(success => {
        this.router.navigate(['addresses']);
      },
      error => {
        console.log(error);
      });
  }
}
