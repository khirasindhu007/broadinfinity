import { DataService } from './../data.service';
import { ApiService } from './../../swagger/api/api.service';
import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes, ActivatedRoute, Router } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
  public countries;
  public states;
  public cities;

  constructor(public api: ApiService, private route: ActivatedRoute, private router: Router, public dataser: DataService) {}

  ngOnInit() {}

  addAddress() {
    this.api.createData(this.countries, this.states, this.cities).subscribe(success => {
      this.router.navigate(['addresses']);
    });
  }

}
