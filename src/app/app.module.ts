import { DataService } from './data.service';
import { AddAddressComponent } from './add-address/add-address.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AddressListComponent } from './address-list/address-list.component';

import { HttpClientModule } from '@angular/common/http';
import { ROUTING } from './app.routing';
import { ApiService } from './../swagger/api/api.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { EditComponent } from './edit/edit.component';

@NgModule({
  declarations: [
    AppComponent,
    AddressListComponent,
    AddAddressComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    ROUTING,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatFormFieldModule,
    FormsModule
  ],
  providers: [ApiService, DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
