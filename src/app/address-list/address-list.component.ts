import { DataService } from './../data.service';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import {
  ApiService
} from './../../swagger/api/api.service';
import {
  Component,
  OnInit
} from '@angular/core';
import swal from 'sweetalert2';


@Component({
  selector: 'app-address-list',
  templateUrl: './address-list.component.html',
  styleUrls: ['./address-list.component.css']
})
export class AddressListComponent implements OnInit {
  public data;
  constructor(public api: ApiService, private route: ActivatedRoute, private router: Router, public dataser: DataService) {}

  ngOnInit() {
    this.api.getAllData().subscribe(success => {
      this.data = success;
    });
  }
  add() {
    this.router.navigate(['addAddress']);
  }
  edit(_id) {
    this.dataser.changeMessage(_id);
    console.log(_id);
    this.router.navigate(['edit']);

  }
  delete(_id) {
    swal({
      title: 'Are you sure?',
      text: 'Are you sure that you want to Delete this?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'No, keep it'
    }).then(result => {
      if (result.value) {
        this.api.deleteData(_id).subscribe(sucess => {
          if (sucess) {
            window.location.reload();
          }
        });
      }
    });
  }
}
